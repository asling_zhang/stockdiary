<%@ page import="com.cch.platform.base.bean.BaseUser"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page import="com.cch.platform.util.*" %>
<%@ page import="com.cch.platform.core.service.PreferenceService" %>
<% //主题
	PreferenceService ps=SpringContext.getBean(PreferenceService.class);
	String theme=ps.getPreference("THEME",(BaseUser)request.getSession().getAttribute("user"));
	if(StringUtil.isEmpty(theme)) theme="classic";
%>
	<script src="${pageContext.request.contextPath}/core/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/core/easyui/jquery.easyui.min.js"></script>
	<script src="${pageContext.request.contextPath}/core/easyui/locale/easyui-lang-zh_CN.js"></script>
	<!--ui扩展-->
	<script src="${pageContext.request.contextPath}/core/jquery/jquery.extend.js"></script>
	<script src="${pageContext.request.contextPath}/core/jquery/jquery.cookie.js"></script>
	<script src="${pageContext.request.contextPath}/core/easyui/extend/easyui.extend.js"></script>
	<script src="${pageContext.request.contextPath}/core/easyui/extend/jquery.edatagrid.js"></script>
	
	<link href="${pageContext.request.contextPath}/core/easyui/themes/<%=theme%>/easyui.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/core/easyui/themes/icon.css" rel="stylesheet">

	<script src="${pageContext.request.contextPath}/core/tool/platform.js"></script>
	<script src="${pageContext.request.contextPath}/core/tool/Data.js"></script>
	<script src="${pageContext.request.contextPath}/core/tool/WinUtil.js"></script>
	<script src="${pageContext.request.contextPath}/core/tool/FlexUtil.js"></script>